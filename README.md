## APPEL

include_once 'WPHelper.php';
$WPHelper = BDSA\WPHelper::getInstance();

//chargement des custom post type
$WPHelper->loadCPTs([
    CPT_PATH . DIRECTORY_SEPARATOR . 'cpt-product.php',
    CPT_PATH . DIRECTORY_SEPARATOR . 'cpt-reference.php',
    CPT_PATH . DIRECTORY_SEPARATOR . 'cpt-realisation.php',
]);

$WPHelper->registerMenus([
    'header_main_menu' => 'Menu Header principal',
    'footer_main_menu' => 'Menu Footer principal',
]);

$WPHelper->addJsScripts([
	[],
	[],
	[],
]);

$WPHelper->addAdminCssFiles([
	[]
]);

etc.