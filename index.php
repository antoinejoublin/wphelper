<?php
/**
 * Plugin Name: BDSA WPHelper
 * Description: Framework Wordpress de développement BDSA
 * Author: BDSA L'Agence
 * Version: 1.0.4
 */
define('PLUGIN_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);
define('PLUGIN_DIRNAME', plugin_basename(__FILE__));
$filepath = PLUGIN_PATH.'WPHelper.php';
$this_file = __FILE__;
$update_check = 'https://scripts.macareux.io/plugins/wordpress/bdsa_helper/update.chk';
require_once PLUGIN_PATH. 'auto-updater.php';
require_once $filepath;

if (file_exists($filepath)) {
    $wp_helper = \BDSA\WPHelper::getInstance();
    add_action('get_template_part', function () use ($wp_helper) {
        set_query_var('wp_helper', $wp_helper);
    });
} else {
    die('Error: ' . $filepath . ' n\'a pas été trouvé.');
}


//http://montivilliers.macareux.io/wp-admin/update.php?action=upgrade-plugin&plugin=davidbdsa-wphelper-8ce70f99b549%2Findex.php&_wpnonce=d6aaef84d1
//
function bdsa_helper_settings_action_links($links, $file)
{

    // lien vers les widgets
    $mylink = '<a href="' . wp_nonce_url(admin_url('update.php?action=upgrade-plugin&plugin='.urlencode(PLUGIN_DIRNAME))) . '">' . __('Demander la mise à jour') . '</a>';
    array_push($links, $mylink);
  /*
     // liens vers les articles
    $links[] = '<a href="' . admin_url( 'edit.php' ) . '">' . __( 'Posts' ) . '</a>';

    // lien vers la page de config de ce plugin
    array_unshift( $links, '<a href="' . admin_url( 'admin.php?page=bawcco_settings' ) . '">' . __( 'Settings' ) . '</a>' );
*/
    return $links;
}
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'bdsa_helper_settings_action_links', 10, 2);
